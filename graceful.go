package graceful

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

type closeHandler struct {
	DoneChans []chan struct{}
}

//NewCloseHandler returns a new  close handler
func NewCloseHandler(cancel context.CancelFunc) *closeHandler {
	ch := &closeHandler{
		DoneChans: make([]chan struct{}, 0),
	}
	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		fmt.Println("\r- Ctrl+C pressed in Terminal")
		cancel()
		ch.closechannels()

	}()
	return ch
}

func (ch *closeHandler) closechannels() {
	for _, c := range ch.DoneChans {
		close(c)
	}
}

func (ch *closeHandler) AddDoneChannel(c chan struct{}) {
	ch.DoneChans = append(ch.DoneChans, c)
}
